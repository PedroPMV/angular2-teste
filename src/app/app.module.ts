import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from './materialModule';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NewPostModalComponent } from './modals/new-post-modal/new-post-modal.component';
import { DeletePostModalComponent } from './modals/delete-post-modal/delete-post-modal.component';
import { EditPostModalComponent } from './modals/edit-post-modal/edit-post-modal.component';

import { DataService } from './services/data.service';
import { PostsService } from './services/posts.service';

@NgModule({
  declarations: [
    AppComponent,
    NewPostModalComponent,
    DeletePostModalComponent,
    EditPostModalComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    FormsModule,
    BrowserAnimationsModule,
    InMemoryWebApiModule.forRoot(DataService),
  ],
  providers: [PostsService],
  entryComponents: [NewPostModalComponent, DeletePostModalComponent, EditPostModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
