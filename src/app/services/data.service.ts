import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api'

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService {

  constructor() { }
  createDb() {

    let posts = [
      { id: 1,  author: 'Pedro', title: 'Teste', content: 'um post de teste', img: 'https://via.placeholder.com/400x200'},
    ];

    return { posts };

  }
}
