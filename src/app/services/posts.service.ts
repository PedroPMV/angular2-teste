import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  SERVER_URL: string = "http://localhost:8080/api/";
  constructor(private httpClient: HttpClient) { }

  public getPosts() {
    return this.httpClient.get(`${this.SERVER_URL}posts/`);
  }
  public createPost(post: { id: number, author: string, title: string, content: string, img: string }) {
    return this.httpClient.post(`${this.SERVER_URL}posts/`, post);
  }
  public deletePost(post) {
    return this.httpClient.delete(`${this.SERVER_URL}posts/${post}`);
  }
  public updatePost(post) {
    return this.httpClient.put(`${this.SERVER_URL}posts/${post.id}`, post);
  }
}

