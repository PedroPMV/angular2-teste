import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-edit-post-modal',
  templateUrl: './edit-post-modal.component.html',
  styleUrls: ['./edit-post-modal.component.sass']
})
export class EditPostModalComponent {

  post: any;
  constructor(
    private postsService: PostsService,
    public dialogRef: MatDialogRef<EditPostModalComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    console.log(data)
    this.post = data.post
  }

  edit() {
    this.postsService.updatePost(this.post).subscribe(data => { console.log(data), this.dialogRef.close(); })
  }
  noClick() {
    this.dialogRef.close();
  }

}
