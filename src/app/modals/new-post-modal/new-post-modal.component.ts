import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-new-post-modal',
  templateUrl: './new-post-modal.component.html',
  styleUrls: ['./new-post-modal.component.sass']
})
export class NewPostModalComponent {
  public post = {
    id: 0,
    author: '',
    title: '',
    content: '',
    img: '',
  }
  constructor(
    public dialogRef: MatDialogRef<NewPostModalComponent>,
    private postsService: PostsService
  ) { }

  create() {
    this.post.id = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
    this.postsService.createPost(this.post).subscribe(data => { console.log(data), this.dialogRef.close(); })
  }
  noClick() {
    this.dialogRef.close();
  }
}
