import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-delete-post-modal',
  templateUrl: './delete-post-modal.component.html',
  styleUrls: ['./delete-post-modal.component.sass']
})
export class DeletePostModalComponent {
  id: string;
  constructor(
    private postsService: PostsService,
    public dialogRef: MatDialogRef<DeletePostModalComponent>,
    @Inject(MAT_DIALOG_DATA) data

  ) { this.id = data.id; console.log(this.id) }

  noClick() {
    this.dialogRef.close();
  }

  yesClick() {
    this.postsService.deletePost(this.id).subscribe(data => {
      this.dialogRef.close();
    })
  }
}
