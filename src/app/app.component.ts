import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { NewPostModalComponent } from './modals/new-post-modal/new-post-modal.component';
import { DeletePostModalComponent } from './modals/delete-post-modal/delete-post-modal.component';
import { EditPostModalComponent } from './modals/edit-post-modal/edit-post-modal.component';

import { PostsService } from './services/posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public dialogConfig = new MatDialogConfig();
  public posts: any
  constructor(public dialog: MatDialog, private postsService: PostsService) {
    this.dialogConfig.width = '400px';
  }
  ngOnInit() {
    this.getPosts();
  }
  // nova de post
  newPost() {
    console.log('alo');
    const dialogRef = this.dialog.open(NewPostModalComponent);
    this.refresh(dialogRef);
  }
  // delete de post
  deletePost(id) {
    this.dialogConfig.data = { id: id };
    const dialogRef = this.dialog.open(DeletePostModalComponent, this.dialogConfig);
    this.refresh(dialogRef);
  }
  // edit de post
  editPost(post) {
    this.dialogConfig.data = { post };
    const dialogRef = this.dialog.open(EditPostModalComponent, this.dialogConfig);
    this.refresh(dialogRef);
  }

  getPosts() {
    this.postsService.getPosts().subscribe(data => { this.posts = data, console.log(data); })
  }
  refresh(ref) {
    ref.afterClosed().subscribe(result => {
      this.getPosts();
    });
  }
}


