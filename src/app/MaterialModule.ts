import { NgModule } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
@NgModule({
    imports: [
        MatIconModule,
        MatCardModule,
        MatDialogModule,
        MatButtonModule,
        MatToolbarModule,
        MatInputModule
    ],
    exports: [
        MatIconModule,
        MatCardModule,
        MatDialogModule,
        MatButtonModule,
        MatToolbarModule,
        MatInputModule
    ],
})
export class MaterialModule { }